/*Selectbox Menu Start*/
$('.select-list').click(function () {
    
    if ( $(this).hasClass("open") == false ){
        
        $(this).toggleClass('open');
        $(this).find('.select-items').stop(true,false).delay(120).slideDown(400);

    }else{

        $(this).removeClass('open');
        $(this).find('.select-items').stop(true,false).slideUp(400);

    }
    
}).focusout(function () {

    $(this).removeClass('open');
    $(this).find('.select-items').stop(true,false).slideUp(400);

});
    // Selectbox Selected
$('.select-list .select-items li').click(function () {

    $(this).parents('.select-list').find('span').text($(this).text());
    $(this).parents('.select-list').find('input').attr('value', $(this).text());
    $(this).parents('.select-list').find(".select-items li").removeClass("active");
    $(this).addClass("active");

});


// $('.select-items li').click(function () {
// var input = '<strong>' + $(this).parents('.dropdown').find('input').val() + '</strong>',
//   msg = '<span class="msg">Hidden input value: ';
// $('.msg').html(msg + input + '</span>');
// }); 


// RequestPost

class RequestPost {

	async post(url, data){
		let postObje = {
				method: 'POST',
				body: data
		}
		const response = await fetch(url, postObje);
        const resData = response.json();
        return resData;
	}
}

const searchForm = document.getElementById("search_form");

searchForm.onsubmit = function(){
    const url = searchForm.action;
    const formData = new FormData(searchForm);
    const req = new RequestPost();

    req.post(url, formData)
    .then(data => console.log(data))
    .catch(err => console.log(error));

    return false;
}


